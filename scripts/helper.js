﻿crmNS.showError = function (container, message) {
    $('#' + container).removeClass('alert-success');
    $('#' + container).addClass('alert-danger');
    $('#' + container).html(message);
    $('#' + container).show();
};

crmNS.showSuccess = function (container, message) {
    $('#' + container).removeClass('alert-danger');
    $('#' + container).addClass('alert-success');
    $('#' + container).html(message);
    $('#' + container).show();
};

crmNS.hideMessage = function (container) {
    $('#' + container).html('');
    $('#' + container).hide();
};


crmNS.displayServerError = function (container, errors, topMsg) {
    var errorMsg = "";
    if (errors !== null) {
        $.each(errors, function (key, val) {
            errorMsg = errorMsg+ val.Message + "<BR/>";
        });
        if (errorMsg !== "") {
            if (topMsg !== "") {
                errorMsg = topMsg + "<br/>" + errorMsg;
            }
            this.showError(container, errorMsg);
        }
    }
};


﻿crmAjax = {};

crmAjax.send = function (params) {
    
    debugger
    var url = params.url;
    if ((url === undefined) || (url === null) || (url === "")) {
        url = crmNS.api.url + "/" + params.service + "/api/" + params.api;
    }
    else {
        url += params.api;
    }

    var method = params.type;
    if ((method === undefined) || (method === null) || (method === "")) {
        method = "POST";
    }

  
   
    var formData = new FormData();
   
    if (params.fileControl !== null && params.fileControl !== undefined && params.fileControl !== "") {
        formData.append('files', $('#' + params.fileControl)[0].files[0]);
        params.dataType = null;
    }

    formData.append("RequestData", params.data);
    console.log(url);
    var ajaxObj = $.ajax({
        url: url,
        data: formData,
        dataType: params.dataType,
        type: method,
        contentType: false,
        processData: false,
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Bearer " + sessionStorage.getItem("accessToken"));
            request.setRequestHeader("ar", "5e3d803410d9620e4882497urtde");
        },
        success: function (data) {
            debugger
            if (data.HasErrors === true) {
                if (params.error !== null) {
                    params.error(false, data, crmNS.consolidateErrors(data));
                }
            }
            else {
                if (params.success !== null) {
                    params.success(true, data, crmNS.consolidateErrors(data));
                }
            }
        },
        error: function (xhr, status, data) {
            debugger
            if (xhr.status === 403) {
                debugger
            }
            else {
                if (params.error !== null) {
                    params.error(false, data, crmNS.consolidateErrors(data));
                }
            }
        }
    });
    return ajaxObj;
};

crmNS.consolidateErrors = function (data)
{
    var errors = data.Errors;
    if (data.HasErrors === true)
    {
       
        if (errors === null || errors === undefined || errors.length === 0) {
            errors = [];
        }

        if (data.Exception !== null && data.Exception !== undefined && data.Exception !== "")
        {
            var error = {};
            error.ErrorNumber = 1;
            error.Message = data.Exception;
            errors.push(error);
        }
    }

    if (errors === null || errors === undefined || errors.length === 0) {
        errors = [];
    }
    return errors;
};

    
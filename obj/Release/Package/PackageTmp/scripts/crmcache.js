﻿crmNS.cache={};
crmNS.cache.set = function (k, v) {
    window.sessionStorage.setItem(k, v);
};

crmNS.cache.get = function (k) {
    window.sessionStorage.getItem(k);
};

crmNS.cache.remove = function (k) {
    window.sessionStorage.removeItem(k);
};

crmNS.cache.clear = function () {
    window.sessionStorage.clear();
};
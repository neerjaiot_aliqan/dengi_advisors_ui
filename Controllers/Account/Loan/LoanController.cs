﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DengiAdvisor_CRM.Core.UI.Controllers.Account.Loan
{
    public class LoanController : Controller
    {
        // GET: Loan
        public ActionResult DailyPickup()
        {
            return View();
        }

        public ActionResult SendToBank()
        {
            return View();
        }

        public ActionResult LoggedIn()
        {
            return View();
        }

        public ActionResult CreditProcess()
        {
            return View();
        }
        public ActionResult ApprovedFile()
        {
            return View();
        }
        public ActionResult DisbursedFile()
        {
            return View();
        }
        public ActionResult RejectedFile()
        {
            return View();
        }
    }
}
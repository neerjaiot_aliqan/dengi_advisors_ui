﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace DengiAdvisor_CRM.Core.UI.Controllers
{

    class person
    {
        public string name { get; set; }
        public string mobile { get; set; }
    }
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult SignIn()
        {
            return View();
        }

        public string Index()
        {
            List<person> data = new List<person>();
           data.Add(new person { name="neeraj",mobile="7017064248"});
            var jsondata = new JavaScriptSerializer().Serialize(data);
            return jsondata;
        }
        public ActionResult SuperAdmin()
        {
            return View();
        }
    }
}
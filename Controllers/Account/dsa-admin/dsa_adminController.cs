﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DengiAdvisor_CRM.Core.UI.Controllers.Account.dsa_admin
{
    public class dsa_adminController : Controller
    {
        // GET: dsa_admin
        public ActionResult Index()
        {
            return View();
        }
       
        public ActionResult account()
        {
            return View();
        }

        public ActionResult dailypickup()
        {
            return View();
        }
        public ActionResult banklogins()
        {
            return View();
        }
        public ActionResult pendencies()
        {
            return View();
        }
        public ActionResult creditprocess()
        {
            return View();
        }
        public ActionResult disbursedfiles()
        {
            return View();
        }
        public ActionResult rejectedfiles()
        {
            return View();
        }
    }
}
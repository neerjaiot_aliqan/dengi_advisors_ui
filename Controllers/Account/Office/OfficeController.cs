﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DengiAdvisor_CRM.Core.UI.Controllers.Account.Office
{
    //[Authorize]
    public class OfficeController : Controller
    {
        // GET: Office
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Registration()
        {
            return View();
        }
        public ActionResult UserRegistration()
        {
            return View();
        }
        public ActionResult UserEmailVerification(string email)
        {
            string []data = email.Split(':');
            ViewBag.UserEmail = data[0];
            ViewBag.usertype = data[1];

            return View();
        }
        public ActionResult success(string email)
        {
            return View();
        }

        public string DecryptString(string encrString)
        {
            byte[] b;
            string decrypted;
            try
            {
                b = Convert.FromBase64String(encrString);
                decrypted = System.Text.ASCIIEncoding.ASCII.GetString(b);
            }
            catch (FormatException fe)
            {
                decrypted = "";
            }
            return decrypted;
        }

        public string EnryptString(string strEncrypted)
        {
            byte[] b = System.Text.ASCIIEncoding.ASCII.GetBytes(strEncrypted);
            string encrypted = Convert.ToBase64String(b);
            return encrypted;
        }

    }
}
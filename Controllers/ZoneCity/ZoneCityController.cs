﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DengiAdvisor_CRM.Core.UI.Controllers.ZoneCity
{
    public class ZoneCityController : Controller
    {
        // GET: ZoneCity
        public ActionResult Zone()
        {
            return View();
        }
        public ActionResult City()
        {
            return View();
        }
        public ActionResult Region()
        {
            return View();
        }
    }
}